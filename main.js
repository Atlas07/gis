const getCoords = item => {
  const coords = item.geometry.coordinates;
  const latLng = new google.maps.LatLng(coords[1], coords[0]);

  return latLng;
}

const showMarker = (item, map) => {
  new google.maps.Marker({
    position: getCoords(item),
    map,
  })
};

const showCircle = (item, map) => {
  const { mag } = item.properties;

  new google.maps.Circle({
    strokeColor: '#FF0000',
    strokeOpacity: 0.8,
    strokeWeight: 2,
    fillColor: '#FF0000',
    fillOpacity: 0.35,
    radius: mag * 1000,
    center: getCoords(item),
    map,
  })
}

const changeGradient = heatmap => {
  const gradient = [
    'rgba(0, 255, 255, 0)',
    'rgba(0, 255, 255, 1)',
    'rgba(0, 191, 255, 1)',
    'rgba(0, 127, 255, 1)',
    'rgba(0, 63, 255, 1)',
    'rgba(0, 0, 255, 1)',
    'rgba(0, 0, 223, 1)',
    'rgba(0, 0, 191, 1)',
    'rgba(0, 0, 159, 1)',
    'rgba(0, 0, 127, 1)',
    'rgba(63, 0, 91, 1)',
    'rgba(127, 0, 63, 1)',
    'rgba(191, 0, 31, 1)',
    'rgba(255, 0, 0, 1)'
  ]

  heatmap.set('gradient', heatmap.get('gradient') ? null : gradient);
}

const changeRadius = heatmap => {
  heatmap.set('radius', heatmap.get('radius') ? null : 20);
}

const changeOpacity = heatmap => {
  heatmap.set('opacity', heatmap.get('opacity') ? null : 0.2);
}

const initMap = () => {
  const mapDOM = document.getElementById('google');
  const map = new google.maps.Map(mapDOM, {
    zoom: 5,
    center: { lat: 2.8, lng: -187.3 },
    matTypeId: 'satellite',
  });

  const script = document.createElement('script');
  script.src = 'https://developers.google.com/maps/documentation/javascript/examples/json/earthquake_GeoJSONP.js';
  document.getElementsByTagName('head')[0].appendChild(script);

  window.eqfeed_callback = results => {
    // results.features.map(item => showCircle(item, map))

    const heatmapData = results.features.map(item => getCoords(item));
    const heatmap = new google.maps.visualization.HeatmapLayer({
      data: heatmapData,
      map: map,
    });

    changeGradient(heatmap);
    changeRadius(heatmap);
    changeOpacity(heatmap);
  };
}

window.onload = () => initMap()
