const getCoords = item => {
  const coords = item.geometry.coordinates;
  const latLng = new google.maps.LatLng(coords[1], coords[0]);

  return latLng;
}

const showMarker = (item, map) => {
  new google.maps.Marker({
    position: getCoords(item),
    map,
  })
};


const initMap = () => {
  const mapDOM = document.getElementById('google');
  const map = new google.maps.Map(mapDOM, {
    zoom: 5,
    center: { lat: 2.8, lng: -187.3 },
  });

  const script = document.createElement('script');
  script.src = 'https://developers.google.com/maps/documentation/javascript/examples/json/earthquake_GeoJSONP.js';
  document.getElementsByTagName('head')[0].appendChild(script);

  window.eqfeed_callback = results => {
    results.features.map(item => showMarker(item, map))
  };
}

window.onload = () => initMap()
